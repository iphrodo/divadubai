$(document).ready(function(){
	$('.main_slider .block_content').slick({
		arrows: false,
		dots: true,
		fade: true,
		autoplay: true,
		autoplaySpeed: 3000,
	});
	$('.services_block .block_content').slick({
		arrows: false,
		dots: true,
	});
	$('.header .header_content .menu_btn').click(function(){
		$(this).next().toggleClass('active');
		$(this).toggleClass('active');
	});
	$(function(){
		$(".field_birth input").datepicker();
	});
	if ($('#form_login').length){
		$('.page_title').addClass('page_login');
	}
});